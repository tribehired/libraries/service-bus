import { ServiceDefinition } from "../contracts";
export declare const serviceRunner: (definition: ServiceDefinition) => Promise<void>;
