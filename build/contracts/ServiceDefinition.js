"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Type;
(function (Type) {
    Type[Type["COMMAND"] = 0] = "COMMAND";
    Type[Type["EVENT"] = 1] = "EVENT";
})(Type = exports.Type || (exports.Type = {}));
