import { serviceRunner, Type } from "../src"

async function main(): Promise<void> {
  console.log("Running sample command handler")
  serviceRunner({
    serviceName: "sample-command",
    connectionSetting: {
      hostname: "localhost",
      port: 5672,
      username: "admin",
      password: "secret",
    },
    stream: "sample.create",
    type: Type.COMMAND,
    handler: async (payload: {msg: string}): Promise<{msg: string, on: string}> => {
      console.log("Command Received: sample.create")
      console.log(payload)
      return {
        ...payload,
        on: new Date().toISOString(),
      }
    },
    emit: "sample.created",
  })
  console.log("Listening to command sample.create")
}

main()
