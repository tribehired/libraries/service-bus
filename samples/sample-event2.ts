import { serviceRunner, Type } from "../src"

async function main(): Promise<void> {
  console.log("Running sample event handler")
  serviceRunner({
    serviceName: "sample-event-2",
    connectionSetting: {
      hostname: "localhost",
      port: 5672,
      username: "admin",
      password: "secret",
    },
    stream: "sample.created",
    type: Type.EVENT,
    handler: async (payload: {msg: string}): Promise<{
      msg: string,
      on: string,
      confirmed: boolean
    }> => {
      console.log("Event Received: sample.created")
      console.log(payload)
      return {
        ...payload,
        on: new Date().toISOString(),
        confirmed: true,
      }
    },
    emit: "sample.confirmed"
  })
  console.log("Listening to event sample.created")
}

main()
