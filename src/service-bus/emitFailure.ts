import * as amqp from "amqplib"
import { ServiceDefinition } from "../contracts"

export const emitFailure = async (conn: amqp.Connection, def: ServiceDefinition, payload: any): Promise<void> => {
  const exhangeName = `${def.emit}.failed`
  const channel = await conn.createChannel()
  const { exchange } = await channel.assertExchange(exhangeName, "fanout", { durable: false })
  channel.publish(exchange, "", Buffer.from(JSON.stringify(payload)))
}
