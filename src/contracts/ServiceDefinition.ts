import { Handler } from "."

export interface ServiceDefinition {
  connectionSetting: {
    hostname: string,
    port: number,
    username: string,
    password: string,
  },
  serviceName: string,
  stream: string,
  type: Type,
  handler: Handler,
  emit?: string,
  emitFailure?: string,
}

export enum Type {
  COMMAND,
  EVENT,
}
